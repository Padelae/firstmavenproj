/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author pjade
 */
public interface CommandHandler {
    
    boolean canProcess(String command);
    String processCommand(String command);
    
}
