public class MathMaven implements CommandHandler {
    private String statement;
    private String name;

    public String getName(String userIn){
        name = userIn;
        return name;
    }
    public void removeChar(String userIn){
        //char[] chars = userIn.toCharArray();
        userIn.trim();
        String newInput = "";

        //loops through the string and adds each character to newInput
        for(int i = 0; i < userIn.length()-1; i++){
            newInput += userIn.charAt(i);
        }

        newInput.trim();
        //checks to make sure that output matches initial input

    }

    //searches for word sequence what is to identify simple math equation
    public int getEqu(String userIn){

        int s = 0;

        for(int i = userIn.length(); i > 0; i--){

            if(userIn.toLowerCase().substring(i-7, i).equals("what is")){
                s = i;
                break;
            }
        }
        return s;
    }

    //identifies string as a number or not
    public boolean isNumber(String userIn){

        if (userIn == null) {
            return false;
        }
        int length = userIn.length();

        if (length == 0) {
            return false;
        }
        int i = 0;

        if (userIn.charAt(0) == '-') {
            if (length == 1) {
                return false;
            }
            i = 1;
        }
        for (; i < length; i++) {
            char c = userIn.charAt(i);

            if (c < '0' || c > '9') {
                return false;
            }
        }
        return true;
    }

    //gets equation left of the operand symbol
    public float getLeftSide(String userIn){

        float leftS = 0;
        String temp = userIn.substring(getEqu(userIn));

        for (int i = temp.length(); i > 0; i-- ){

            if(temp.substring(i-2, i).contains("+")||
                    temp.substring(i-2, i).contains("-")||
                    temp.substring(i-2, i).contains("/")||
                    temp.substring(i-2, i).contains("*")||
                    temp.substring(i-2, i).contains("^")){
                leftS = Float.parseFloat(temp.substring(0, i-2));
                break;
            }
        }

        return leftS;
    }

    //returns numbers right of operand symbol
    public float getRightSide(String userIn){

        float RightS = 0;
        String temp = userIn.substring(getEqu(userIn));

        for (int i = temp.length(); i > 0; i-- ){

            if(temp.substring(i-2, i).contains("+")||
                    temp.substring(i-2, i).contains("-")||
                    temp.substring(i-2, i).contains("/")||
                    temp.substring(i-2, i).contains("*")||
                    temp.substring(i-2, i).contains("^")){
                RightS = Float.parseFloat(temp.substring(i-1));
                break;
            }
        }

        return RightS;
    }

        //checks to see if there is an operand in the string
    public boolean checker(String userIn){
        if(userIn.contains("+") || userIn.contains("-") || userIn.contains("/") ||
                userIn.contains("*") || userIn.contains("^")){
            return true;
        }else{
            return false;
        }
    }

    //does the intended operation in a string after isolating the left and right side
    public float operation(String userIn){

        float ans = 0;
        String temp = userIn.substring(getEqu(userIn));

        if(temp.contains("+")){
            ans = getRightSide(userIn) + getLeftSide(userIn);
        }

        if(temp.contains("-")){
            ans = getLeftSide(userIn) - getRightSide(userIn);
        }

        if(temp.contains("*")){
            ans = getLeftSide(userIn) * getRightSide(userIn);
        }

        if(temp.contains("/")){
            ans = getLeftSide(userIn) / getRightSide(userIn);
        }

        if(temp.contains("^")){
            ans = (float) Math.pow(getLeftSide(userIn),getRightSide(userIn)) ;
        }

        return ans;
    }

//gets value after = sign
    public String getTotal(String userIn){
        String end = "";
        for( int i = userIn.length() - 1; i > 0; i--){
            if(userIn.substring(i,i+1).contains("=")){
                end = userIn.substring(i+1);
            }
        }
        return end;
    }

    //returns the known number value and solves for variable
    public float findNumber(String userIn) {
        float num = 0;
        int i = userIn.indexOf("=");
        int l = 0;
        if (userIn.contains("=")) {
            if (userIn.contains("/")) {
                l = userIn.indexOf("/");
                num = Float.parseFloat(userIn.substring(l+1,i));
                num = Float.parseFloat(getTotal(userIn))*num;
            }
            if (userIn.contains("+")) {
            l = userIn.indexOf("+");
                num = Float.parseFloat(userIn.substring(l+1,i));
                num = Float.parseFloat(getTotal(userIn)) - num;
        }
            if (userIn.contains("*")) {
            l = userIn.indexOf("*");
                num = Float.parseFloat(userIn.substring(l+1,i));
                num = Float.parseFloat(getTotal(userIn))/num;
        }
            if (userIn.contains("-")) {
            l = userIn.indexOf("-");
                num = Float.parseFloat(userIn.substring(l+1,i));
                num = Float.parseFloat(getTotal(userIn)) + num;
        }
            }

        return num;
    }

    //returns a statement based off the user input
    public String getResponse(String userIn){
        statement = "";
        removeChar(userIn);

        //returns the string if it is just a number
        if(isNumber(userIn)){
            statement = "The answer is " + Float.valueOf(userIn);
        }

        //returns the output of an equation
        if(userIn.toLowerCase().contains("what is ")) {
            statement = userIn.substring(getEqu(userIn)) + " equals " +
                    operation(userIn);
        }

        /**solves for x only if it follows the "x + r = j" where "+" can be -, +, /, *. r can be any positive integer
         * j can be any positive integer. EX. Input "Solve x * 9 = 22"
        **/
        if(userIn.toLowerCase().contains("solve ")){
            statement ="x = " + findNumber(userIn);
        }
        return statement;
    }

    //implemented handler to check if user input can be handled by file
    @Override
    public boolean canProcess(String command) {
        return true;
    }

    //runs code if canProcess returns true
    @Override
    public String processCommand(String command) {
        return getResponse(command);
    }
}

